﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parallel
{
    public  class MyThreads
    {


        Barrier barrier;
        long sum = 0;

        public void Compute(int[] array)
        {

            
            var sw = new Stopwatch();            
            int cores = Environment.ProcessorCount*2;
            barrier = new Barrier(cores + 1);
            int part = array.Length / cores;
            var subarrays = Split(array, part).ToArray();

            sw.Start();
            for (int c = 0; c < cores; c++)
            {
                Thread t = new Thread(GetSum);
                t.Start(subarrays[c]);
            }
            barrier.SignalAndWait();
            sw.Stop();
            Console.WriteLine($"{cores} thread result: sum={sum}, elapsed: {sw.ElapsedMilliseconds} ms");
        }


        public void GetSum(object obj)
        {
            var array = ((IEnumerable<int>)obj).ToArray();
            long subsum = 0;
              for (int i = 0; i < array.Length; i++)
                subsum += array[i];
            Interlocked.Add(ref sum, subsum);
            barrier.SignalAndWait();
        }


        public IEnumerable<IEnumerable<int>> Split(int[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }
    }
}
