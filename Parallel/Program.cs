﻿using System.Diagnostics;
using System.Numerics;

namespace Parallel
{
    public class Program
    {
        
        static void  Main(string[] args)
        {
            Console.WriteLine("Type number of elements:");
            var n = Int32.Parse( Console.ReadLine());

            int[] array = new int[n];
            long sum = 0;

            for (int i = 0; i < n; i++)
                array[i] = i * i - 1000 * i;

            var sw = new Stopwatch();

            sw.Start();
            for (int i = 0; i < n; i++)
                sum += array[i];
            sw.Stop();
            Console.WriteLine($"1 thread result: sum={sum}, elapsed: {sw.ElapsedMilliseconds} ms");


            var t = new MyThreads();
            t.Compute(array);


            //LINQ
            sum = 0;
            sw.Restart();
            array.AsParallel().ForAll(MyFunc);
            sw.Stop();
            Console.WriteLine($"LINQ result: sum={sum}, elapsed: {sw.ElapsedMilliseconds} ms");

            void MyFunc(int l) => Interlocked.Add(ref sum,l);


            Console.ReadKey();




        }





    }
}